-- ========================================================================== --
-- ==                       UTILITIES (functions, etc)                     == --
-- ========================================================================== --

local M = {} -- The module to export
local cmd = vim.cmd

function M.on_load(name, fn)
	local Config = require("lazy.core.config")
	if Config.plugins[name] and Config.plugins[name]._.loaded then
		fn(name)
	else
		vim.api.nvim_create_autocmd("User", {
			pattern = "LazyLoad",
			callback = function(event)
				if event.data == name then
					fn(name)
					return true
				end
			end,
		})
	end
end

function M._echo_multiline(msg)
	for _, s in ipairs(vim.fn.split(msg, "\n")) do
		cmd("echom '" .. s:gsub("'", "''") .. "'")
	end
end

function M.info(msg)
	cmd("echohl Directory")
	M._echo_multiline(msg)
	cmd("echohl None")
end

function M.warn(msg)
	cmd("echohl WarningMsg")
	M._echo_multiline(msg)
	cmd("echohl None")
end

function M.err(msg)
	cmd("echohl ErrorMsg")
	M._echo_multiline(msg)
	cmd("echohl None")
end

function M.make_directory(name)
	local dir = vim.fn.expand(name)
	if vim.fn.isdirectory(dir) == 0 then
		vim.fn.mkdir(dir, "p")
	end
end

-- We will create a few autogroup, this function will help to avoid
-- always writing cmd('augroup' .. group) etc..
function M.create_augroup(autocmds, name)
	cmd("augroup " .. name)
	cmd("autocmd!")
	for _, autocmd in ipairs(autocmds) do
		cmd("autocmd " .. table.concat(autocmd, " "))
	end
	cmd("augroup END")
end

-- Map a key with optional options
function M.map(mode, keys, action, options)
	if options == nil then
		options = {}
	end
	vim.api.nvim_set_keymap(mode, keys, action, options)
end

-- Map a key to a lua callback
function M.map_lua(mode, keys, action, options)
	if options == nil then
		options = {}
	end
	vim.api.nvim_set_keymap(mode, keys, "<cmd>lua " .. action .. "<cr>", options)
end

-- Buffer local mappings
function M.map_buf(mode, keys, action, options, buf_nr)
	if options == nil then
		options = {}
	end
	local buf = buf_nr or 0
	vim.api.nvim_buf_set_keymap(buf, mode, keys, action, options)
end

function M.map_lua_buf(mode, keys, action, options, buf_nr)
	if options == nil then
		options = {}
	end
	local buf = buf_nr or 0
	vim.api.nvim_buf_set_keymap(buf, mode, keys, "<cmd>lua " .. action .. "<cr>", options)
end

function M.EscapePair()
	local closers = { ")", "]", "}", ">", "'", '"', "`", "," }
	local line = vim.api.nvim_get_current_line()
	local row, col = unpack(vim.api.nvim_win_get_cursor(0))
	local after = line:sub(col + 1, -1)
	local closer_col = #after + 1
	local closer_i = nil
	for i, closer in ipairs(closers) do
		local cur_index, _ = after:find(closer)
		if cur_index and (cur_index < closer_col) then
			closer_col = cur_index
			closer_i = i
		end
	end
	if closer_i then
		vim.api.nvim_win_set_cursor(0, { row, col + closer_col })
	else
		vim.api.nvim_win_set_cursor(0, { row, col + 1 })
	end
end

-- Toggle the Diagnostics float
local DiagsActive = false
function M.ToggleDiagnostics()
	DiagsActive = not DiagsActive
	if DiagsActive then
		M.create_augroup({
			{ "CursorHold", "*", ":lua vim.diagnostic.open_float()" },
		}, "GroupCursorHold")
	else
		cmd("augroup GroupCursorHold")
		cmd("autocmd!")
		cmd("augroup END")
	end
end

function M.close_floating_windows()
	local inactive_floating_wins = vim.fn.filter(vim.api.nvim_list_wins(), function(k, v)
		local file_type = vim.api.nvim_buf_get_option(vim.api.nvim_win_get_buf(v), "filetype")

		return vim.api.nvim_win_get_config(v).relative ~= ""
			and v ~= vim.api.nvim_get_current_win()
			and file_type ~= "hydra_hint"
	end)
	for _, w in ipairs(inactive_floating_wins) do
		pcall(vim.api.nvim_win_close, w, false)
	end
end

-- Run command with sudo
M.sudo_exec = function(sudo_cmd, print_output)
	vim.fn.inputsave()
	local password = vim.fn.inputsecret("Password: ")
	vim.fn.inputrestore()
	if not password or #password == 0 then
		M.warn("Invalid password, sudo aborted")
		return false
	end
	local out = vim.fn.system(string.format("sudo -p '' -S %s", sudo_cmd), password)
	if vim.v.shell_error ~= 0 then
		print("\r\n")
		M.err(out)
		return false
	end
	if print_output then
		print("\r\n", out)
	end
	return true
end

-- Write the file even if it is Read-Only
M.sudo_write = function(tmpfile, filepath)
	if not tmpfile then
		tmpfile = vim.fn.tempname()
	end
	if not filepath then
		filepath = vim.fn.expand("%")
	end
	if not filepath or #filepath == 0 then
		M.err("E32: No file name")
		return
	end
	-- `bs=1048576` is equivalent to `bs=1M` for GNU dd or `bs=1m` for BSD dd
	-- Both `bs=1M` and `bs=1m` are non-POSIX
	local sudo_cmd =
		string.format("dd if=%s of=%s bs=1048576", vim.fn.shellescape(tmpfile), vim.fn.shellescape(filepath))
	-- no need to check error as this fails the entire function
	vim.api.nvim_exec(string.format("write! %s", tmpfile), true)
	if M.sudo_exec(sudo_cmd) then
		M.info(string.format([["%s" written]], filepath))
		cmd("e!")
	end
	vim.fn.delete(tmpfile)
end

-- We want to be able to access utils in all our configuration files
-- so we add the module to the _G global variable.

_G.utils = M
return M -- Export the module
