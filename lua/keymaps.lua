-- ========================================================================== --
-- ==                           KEYMAPS BINDING                            == --
-- ========================================================================== --

local options = { noremap = true, silent = true }

vim.g.mapleader = ","
vim.g.maplocalleader = ","

-- Faster write and quit
utils.map("n", "<leader>w", "<cmd>write<cr>", options)
utils.map("n", "<leader>ww", "<cmd>wa<cr>", options)
utils.map("n", "<leader>wq", "<cmd>write<cr><cmd>quit<cr>", options)
utils.map("n", "<leader>qq", "<cmd>qa!<cr>", options)

-- Redo :: I prefer to use U than <C-r>
utils.map("n", "U", "<C-r>", options)

-- Yank to system clipboard
utils.map("n", "<leader>y", '"+y', options)
utils.map("n", "<leader>Y", '"+yg_', options)
utils.map("v", "<leader>y", '"+y', options)

-- Paste from system clipboard
utils.map("n", "<leader>p", '"+p', options)
utils.map("n", "<leader>P", '"+P', options)
utils.map("v", "<leader>p", '"+p', options)
utils.map("v", "<leader>P", '"+P', options)

-- Navigate through buffers
utils.map("n", "<A-Left>", "<cmd>bprevious<cr>", options)
utils.map("n", "<A-Right>", "<cmd>bnext<cr>", options)
utils.map("n", "<A-Down>", "<cmd>bdelete<cr>", options)

-- Navigate through Windows
utils.map("n", "<C-Left>", "<cmd>wincmd h<cr>", options)
utils.map("n", "<C-Right>", "<cmd>wincmd l<cr>", options)
utils.map("n", "<C-Up>", "<cmd>wincmd k<cr>", options)
utils.map("n", "<C-Down>", "<cmd>wincmd j<cr>", options)

-- Window Resize
utils.map("n", "<S-C-Up>", "<cmd>resize -2<CR>", options)
utils.map("n", "<S-C-Down>", "<cmd>resize +2<CR>", options)
utils.map("n", "<S-C-Left>", "<cmd>vertical :resize +2<CR>", options)
utils.map("n", "<S-C-Right>", "<cmd>vertical :resize -2<CR>", options)

-- BackSpace = no highlighting
utils.map("n", "<BS>", "<cmd>nohlsearch<cr>", options)

-- F5/F6 toggle line numbers
utils.map("n", "<F5>", "<cmd>set number!<cr>", options)
utils.map("n", "<F6>", "<cmd>set relativenumber!<cr>", options)

-- Leader + s = toggle spellcheck
utils.map("n", "<leader>s", "<cmd>set nospell!<cr>", options)

-- Highlight line/column
utils.map("n", "<leader>hh", "<cmd>set cursorline! cursorcolumn!<cr>", options)
utils.map("n", "<leader>hl", "<cmd>set cursorline!<cr>", options)
utils.map("n", "<leader>hc", "<cmd>set cursorcolumn!<cr>", options)

-- When jump to next match also center screen
utils.map("n", "n", "<cmd>norm! nzz<cr>", options)
utils.map("n", "N", "<cmd>norm! Nzz<cr>", options)
utils.map("v", "n", "<cmd>norm! nzz<cr>", options)
utils.map("v", "N", "<cmd>norm! Nzz<cr>", options)

-- Same when moving Up/Down
utils.map("n", "<C-u>", "<C-u>zz", options)
utils.map("n", "<C-d>", "<C-d>zz", options)
utils.map("n", "<C-f>", "<C-f>zz", options)
utils.map("n", "<C-b>", "<C-b>zz", options)
utils.map("v", "<C-u>", "<C-u>zz", options)
utils.map("v", "<C-d>", "<C-d>zz", options)
utils.map("v", "<C-f>", "<C-f>zz", options)
utils.map("v", "<C-b>", "<C-b>zz", options)

-- Visual --
-- Stay in visual mode
utils.map("v", "<", "<gv", options)
utils.map("v", ">", ">gv", options)

-- Mappings for diff mode
if vim.api.nvim_win_get_option(0, "diff") then
	utils.map("n", "<leader>1", "<cmd>diffget LOCAL<cr>", options)
	utils.map("n", "<leader>2", "<cmd>diffget BASE<cr>", options)
	utils.map("n", "<leader>3", "<cmd>diffget REMOTE<cr>", options)
	utils.create_augroup({
		{ "BufWritePost", "*", "diffupdate" },
	}, "GroupDiffUpdate")
end

-- Find merge conflict markers
utils.map("n", "<leader>fc", "/\v^[<|=>]{7}( .*|$)<cr>", options)

-- You jump out pairs or quotes in insert mode
utils.map("i", "<C-l>", "<cmd>lua utils.EscapePair()<cr>", options)

-- DIAGNOSTICS Related Mappings
-- Toggle Diagnostic
utils.map("n", "<leader>tt", "<cmd>lua utils.ToggleDiagnostics()<cr>", options)
-- Jump to the definition
utils.map("n", "gd", "<cmd>lua vim.lsp.buf.definition()<cr>", options)
-- Jump to declaration
utils.map("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<cr>", options)
-- Lists all the implementations for the symbol under the cursor
utils.map("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<cr>", options)
-- Jumps to the definition of the type symbol
utils.map("n", "go", "<cmd>lua vim.lsp.buf.type_definition()<cr>", options)
-- Lists all the references
utils.map("n", "gr", "<cmd>lua vim.lsp.buf.references()<cr>", options)
-- Displays a function's signature information
utils.map("n", "<C-k>", "<cmd>lua vim.lsp.buf.signature_help()<cr>", options)
-- Renames all references to the symbol under the cursor
utils.map("n", "<F2>", "<cmd>lua vim.lsp.buf.rename()<cr>", options)
-- Selects a code action available at the current cursor position
utils.map("n", "<F4>", "<cmd>lua vim.lsp.buf.code_action()<cr>", options)
utils.map("x", "<F4>", "<cmd>lua vim.lsp.buf.range_code_action()<cr>", options)
-- Show diagnostics in a floating window
utils.map("n", "gl", "<cmd>lua vim.diagnostic.open_float()<cr>", options)
-- Move to the previous diagnostic
utils.map("n", "[d", "<cmd>lua vim.diagnostic.goto_prev()<cr>", options)
-- Move to the next diagnostic
utils.map("n", "]d", "<cmd>lua vim.diagnostic.goto_next()<cr>", options)

-- Write the file even if it is Read-Only
utils.map("c", "w!!", ":lua utils.sudo_write()<cr>", options)

-- Register new Format command
vim.cmd("command! Format execute 'lua vim.lsp.buf.format { async = true }'")

-- Format using language formatting server
utils.map("n", "<leader>F", "<cmd>Format<cr>", options)
utils.map("x", "<leader>F", "<cmd>Format<cr>", options)

-- Show LSP Info
utils.map("n", "<leader>li", "<cmd>LspInfo<cr>", options)

-- Close all floating windows
utils.map("n", "<Esc>", ":lua utils.close_floating_windows()<cr>", options)
