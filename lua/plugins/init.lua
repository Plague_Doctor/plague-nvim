-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"folke/lazy.nvim"           :: 💤 A modern plugin manager for Neovim
"dstein64/vim-startuptime"  :: A plugin for profiling Vim and Neovim startup time
"nvim-lua/plenary.nvim"     :: All the lua functions you don't want to write twice
]]

return {
	{
		"folke/lazy.nvim",
		version = "*",
	},
	{
		"dstein64/vim-startuptime",
		cmd = "StartupTime",
		config = function()
			vim.g.startuptime_tries = 10
		end,
	},
	{
        "nvim-lua/plenary.nvim",
        lazy = true,
    },
}
