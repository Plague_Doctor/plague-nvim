-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --

--[[
"folke/trouble.nvim"    :: A pretty diagnostics, references, telescope results, etc
    <leader>xx            :: Toggle diagnostics
    <leader>xw            :: Toggle workspace diagnostics
    <leader>xd            :: Toggle document diagnostics
]]

return {
	"folke/trouble.nvim",
	dependencies = { "nvim-tree/nvim-web-devicons" },
	keys = {
        { "<leader>xx", "<cmd>TroubleToggle<cr>", desc = "Toggle Trouble" },
        { "<leader>xw", "<cmd>TroubleToggle workspace_diagnostics<cr>", desc = "Workspace Diagnostics" },
        { "<leader>xd", "<cmd>TroubleToggle document_diagnostics<cr>", desc = "Document Diagnostics" },
	},
	opts = {
	    auto_close = true
	},
}
