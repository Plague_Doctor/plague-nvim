-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"sudormrfbin/cheatsheet.nvim"   :: A cheatsheet plugin for neovim
]]

return {
	"sudormrfbin/cheatsheet.nvim",
	event = { "VeryLazy" },
}
