-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"Wansmer/treesj"    :: Neovim plugin for splitting/joining blocks of code
    <S-s>   :: Join or Split
]]

return {
	"Wansmer/treesj",
	keys = {
		{ "<S-s>", "<cmd>TSJToggle<cr>", desc = "Join or Split" },
	},
	config = true
}
