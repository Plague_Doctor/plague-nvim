-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"numToStr/Comment.nvim"     :: Easily comment stuff
    gcc         :: Toggle comment
    <leader>c   :: Toggle comment
]]

return {
	"numToStr/Comment.nvim",
	lazy = false,
	opts = {
		-- add any options here
	},
	keys = {
		{
			"<leader>c",
			"<Plug>(comment_toggle_linewise_current)",
			desc = "Toggle comment on current line",
		},
		{
			"<leader>c",
			"<Plug>(comment_toggle_linewise_visual)",
			mode = "x",
			desc = "Toggle comment on visual block",
		},
	},
}
