-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --

--[[
"altermo/ultimate-autopair.nvim"    :: A autopair plugin with all the plugin needs.
]]

return {
	"altermo/ultimate-autopair.nvim",
	event = { "InsertEnter", "CmdlineEnter" },
	opts = {},
}
