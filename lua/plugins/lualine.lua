-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"nvim-lualine/lualine.nvim"     :: A blazing fast statusline
]]

return {
	"nvim-lualine/lualine.nvim",
	event = { "VeryLazy" },
	init = function()
		vim.g.lualine_laststatus = vim.o.laststatus
		if vim.fn.argc(-1) > 0 then
			-- set an empty statusline till lualine loads
			vim.o.statusline = " "
		else
			-- hide the statusline on the starter page
			vim.o.laststatus = 0
		end
	end,
	opts = function()
		vim.fn.matchadd("errorMsg", [[s+$]])

		local function trailing()
			local space = vim.fn.search([[\s\+$]], "nwc")
			return space ~= 0 and "WS:" .. space or ""
		end

		return {
			options = {
				theme = "auto",
				diagnostics_color = {
					hint = { fg = "#98BB6C" },
					info = { fg = "#6A9589" },
					warn = { fg = "#FF9E3B" },
					error = { fg = "#E82424" },
				},
			},
			sections = {
				lualine_a = { "mode" },
				lualine_b = {
					{
						"branch",
						color = { bg = "#363646" },
					},
					{
						"diff",
						separator = { right = '' },
						color = { bg = "#363646" },
					},
				},
				lualine_c = {
					{
						"filename",
						path = 1,
					},
				},
				lualine_x = { "filetype" },
				lualine_y = {
					{
						"progress",
						color = { bg = "#363646" },
					},
					{
						"location",
						color = { bg = "#363646" },
					},
				},
				lualine_z = {
					{
						"diagnostics",
						color = { bg = "#363646" },
					},
					{
						trailing,
						separator = { left = "" },
						color = { fg = "white", bg = "#C34043", gui = "bold" },
					},
				},
			},
			tabline = {
				lualine_a = { "buffers" },
			},
			extensions = { "neo-tree" },
		}
	end,
}
