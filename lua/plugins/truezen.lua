-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"Pocco81/true-zen.nvim"     :: Clean and elegant distraction-free writing for NeoVim
    <leader>z   :: Enter Ataraxis mode

"folke/twilight.nvim"       :: Lua plugin for Neovim that dims inactive portions of the code
]]

return {
	{
		"Pocco81/true-zen.nvim",
		dependencies = {
			"folke/twilight.nvim",
		},
		event = { "VeryLazy" },
		keys = {
			{
				"<leader>z",
				"<cmd>TZAtaraxis<cr>",
				desc = "Ataraxis mode",
			},
		},
		opts = {
			modes = { -- configurations per mode
				ataraxis = {
					shade = "light", -- if `dark` then dim the padding windows, otherwise if it's `light` it'll brighten said windows
					backdrop = 0, -- percentage by which padding windows should be dimmed/brightened. Must be a number between 0 and 1. Set to 0 to keep the same background color
					minimum_writing_area = { -- minimum size of main window
						width = 70,
						height = 44,
					},
					quit_untoggles = true, -- type :q or :qa to quit Ataraxis mode
					padding = { -- padding windows
						left = 45,
						right = 45,
						top = 2,
						bottom = 2,
					},
					callbacks = { -- run functions when opening/closing Ataraxis mode
						open_pre = function()
							vim.cmd([[
                                    :set wrap
                                    :set spell
                                    :set scrolloff=999
                                ]])
						end,
						open_pos = nil,
						close_pre = nil,
						close_pos = function()
							vim.cmd([[
                                    :set nowrap
                                    :set nospell
                                    :set scrolloff=3
                                ]])
						end,
					},
					options = { -- options to be disabled when entering Ataraxis mode
						number = false,
						relativenumber = false,
						showtabline = 0,
						signcolumn = "no",
						statusline = "",
						cmdheight = 1,
						laststatus = 0,
						showcmd = false,
						showmode = false,
						ruler = false,
						numberwidth = 1,
					},
				},
				minimalist = {
					ignored_buf_types = { "nofile" }, -- save current options from any window except ones displaying these kinds of buffers
					options = { -- options to be disabled when entering Minimalist mode
						number = false,
						relativenumber = false,
						showtabline = 0,
						signcolumn = "no",
						statusline = "",
						cmdheight = 1,
						laststatus = 0,
						showcmd = false,
						showmode = false,
						ruler = false,
						numberwidth = 1,
					},
					callbacks = { -- run functions when opening/closing Minimalist mode
						open_pre = nil,
						open_pos = nil,
						close_pre = nil,
						close_pos = nil,
					},
				},
				narrow = {
					--- change the style of the fold lines. Set it to:
					--- `informative`: to get nice pre-baked folds
					--- `invisible`: hide them
					--- function() end: pass a custom func with your fold lines. See :h foldtext
					folds_style = "informative",
					run_ataraxis = true, -- display narrowed text in a Ataraxis session
					callbacks = { -- run functions when opening/closing Narrow mode
						open_pre = nil,
						open_pos = nil,
						close_pre = nil,
						close_pos = nil,
					},
				},
				focus = {
					callbacks = { -- run functions when opening/closing Focus mode
						open_pre = nil,
						open_pos = nil,
						close_pre = nil,
						close_pos = nil,
					},
				},
			},
			integrations = {
				lualine = true, -- hide nvim-lualine (ataraxis)
				twilight = true, -- Twilight.nvim integration
			},
		},
	},
	{
		"folke/twilight.nvim",
		opts = {},
	},
}
