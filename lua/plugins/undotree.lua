-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"jiaoshijie/undotree"   :: Undo management
    <leader>u   :: open undotree
]]

return {
	"jiaoshijie/undotree",
	keys = {
		{ "<leader>u", "<cmd>lua require('undotree').toggle()<cr>" },
	},
	opts = {
		float_diff = false, -- using float window previews diff, set this `true` will disable layout option
		layout = "left_bottom", -- "left_bottom", "left_left_bottom"
		ignore_filetype = { "Undotree", "UndotreeDiff", "qf", "TelescopePrompt", "spectre_panel", "tsplayground" },
		window = {
			winblend = 30,
		},
		keymaps = {
			["<Down>"] = "move_change_next",
			["<Up>"] = "move_change_prev",
			["J"] = "move_next",
			["K"] = "move_prev",
			["<cr>"] = "action_enter",
			["p"] = "enter_diffbuf",
			["q"] = "quit"
		},
	},
}
