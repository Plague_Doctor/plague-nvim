-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"echasnovski/mini.surround"     :: Management of brackets
    sa          :: Add surrounding (in visual mode or on motion)
    sd          :: Delete surrounding
    sr          :: Replace surrounding
    sf or sF    :: Find surrounding (move cursor right or left)
    sh          :: Highlight surrounding
    sn          :: Change number of neighbor lines (see |MiniSurround-algorithm|)

"echasnovski/mini.align"        :: Neovim Lua plugin to align text interactively
    ga          :: Enter mini.align interactive mode
    gA          :: Enter mini.align interactive mode (with preview)

"echasnovski/mini.trailspace"   :: Management of trailspaces
    <leader>ws  :: Trim whitespaces
    <leader>wd  :: Trim whitespaces and lines

"echasnovski/mini.move"         :: Move any selection in any direction
    <S-Left>    :: Move line or selection left
    <S-Right>   :: Move line or selection right
    <S-Down>    :: Move line or selection down
    <S-Up>      :: Move line or selection up

"echasnovski/mini.indentscope"  :: Visualize and operate on indent scope
]]

return {
	{
		"echasnovski/mini.surround",
		event = { "VeryLazy" },
		config = true,
	},
	{
		"echasnovski/mini.align",
		event = { "VeryLazy" },
		config = true,
	},
	{
		"echasnovski/mini.trailspace",
		event = { "VeryLazy" },
		version = false,
		config = true,
		keys = {
			{
				"<leader>ws",
				"<cmd>lua MiniTrailspace.trim()<cr>",
				desc = "Trim whitespaces",
			},
			{
				"<leader>wd",
				"<cmd>lua MiniTrailspace.trim()<cr><cmd>lua MiniTrailspace.trim_last_lines()<cr>",
				desc = "Trim whitespaces and lines",
			},
		},
	},
	{
		"echasnovski/mini.move",
		event = { "VeryLazy" },
		opts = {
			mappings = {
				-- Move visual selection in Visual mode. Defaults are Alt (Meta) + hjkl.
				left = "<S-Left>",
				right = "<S-Right>",
				down = "<S-Down>",
				up = "<S-Up>",

				-- Move current line in Normal mode
				line_left = "<S-Left>",
				line_right = "<S-Right>",
				line_down = "<S-Down>",
				line_up = "<S-Up>",
			},
		},
	},
	{
		"echasnovski/mini.indentscope",
		event = { "VeryLazy" },
		opts = {
			symbol = "╎",
			options = { try_as_border = true },
		},
		init = function()
			vim.api.nvim_create_autocmd("FileType", {
				pattern = {
					"help",
					"alpha",
					"dashboard",
					"neo-tree",
					"Trouble",
					"trouble",
					"lazy",
					"mason",
					"notify",
					"toggleterm",
					"lazyterm",
				},
				callback = function()
					vim.b.miniindentscope_disable = true
				end,
			})
		end,
	},
}
