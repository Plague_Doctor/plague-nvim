-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"mhartington/oceanic-next" :: OceanicNext colour scheme
"shaunsingh/nord.nvim"     :: Neovim theme based off of the Nord Color Palette
"rebelot/kanagawa.nvim"    :: colorscheme inspired by Katsushika Hokusai paintings
]]

return {
	{
		"mhartington/oceanic-next",
		priority = 1000,
	},
	{
		"shaunsingh/nord.nvim",
		priority = 1000,
	},
	{
		"catppuccin/nvim",
		name = "catppuccin",
		priority = 1000,
		opts = {
			integrations = {
				cmp = true,
				gitsigns = true,
				nvimtree = true,
				treesitter = true,
				notify = false,
				mini = {
					enabled = true,
					indentscope_color = "",
				},
			},
		},
	},
	{
		"rebelot/kanagawa.nvim",
		priority = 1000,
		opts = {
			theme = "wave",
			background = {
				dark = "wave",
				light = "lotus",
			},
			dimInactive = false,
			globalStatus = true,
			colors = {
				palette = {
					sumiInk0 = "#363646",
				},
				theme = {
					all = {
						ui = {
							bg_gutter = "none",
						},
					},
				},
			},
			overrides = function(colors)
				local theme = colors.theme
				return {
					CmpPmenu = { bg = "#2A2A37" },
					LazyNormal = { bg = theme.ui.bg_m3, fg = theme.ui.fg_dim },
					MasonNormal = { bg = theme.ui.bg_m3, fg = theme.ui.fg_dim },
					TelescopeNormal = { bg = colors.bg_dim },
					TelescopeBorder = { fg = colors.bg_dim, bg = colors.bg_dim },
					TelescopeTitle = { fg = colors.bg_light3, bold = true },
					TelescopePromptNormal = { bg = colors.bg_light0 },
					TelescopePromptBorder = { fg = colors.bg_light0, bg = colors.bg_light0 },
					TelescopeResultsNormal = { bg = "#2A2A37" },
					TelescopeResultsBorder = { fg = colors.bg_light0, bg = colors.bg_light0 },
					TelescopePreviewNormal = { bg = "#363646" },
					TelescopePreviewBorder = { fg = colors.bg_dim, bg = colors.bg_dim },
				}
			end,
		},
	},
}
