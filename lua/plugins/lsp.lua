-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"neovim/nvim-lspconfig"             :: Quickstart configs for Nvim LSP
"williamboman/mason.nvim"           :: Portable package manager for Neovim
"jay-babu/mason-null-ls.nvim"       :: Bridges mason.nvim with the null-ls plugin
"williamboman/mason-lspconfig.nvim" :: Extension to mason.nvim
"nvimtools/none-ls.nvim"            :: Use Neovim as a language server
"hinell/lsp-timeout.nvim"           :: Start/stop LSP servers upon demand
"j-hui/fidget.nvim"                 :: Standalone UI for nvim-lsp progress
]]

return {
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			"williamboman/mason.nvim",
			"jay-babu/mason-null-ls.nvim",
			"williamboman/mason-lspconfig.nvim",
			"nvimtools/none-ls.nvim",
		},
		config = function()
			local mason_lspconfig_status_ok, _ = pcall(require, "mason-lspconfig")
			if not mason_lspconfig_status_ok then
				return
			end
			require("mason-lspconfig").setup({
				ensure_installed = {
					-- LSP servers
					"ansiblels",
					"cmake",
					"dockerls",
					"eslint",
					"jsonls",
					"tsserver",
					"lua_ls",
					"marksman",
					"jedi_language_server",
					"yamlls",
					"vimls",
					"bashls",
				},
				automatic_installation = true,
			})
			require("mason-lspconfig").setup_handlers({
				function(server_name) -- default handler (optional)
					require("lspconfig")[server_name].setup({})
				end,
			})

			local mason_null_ls_status_ok, _ = pcall(require, "mason-null-ls")
			if not mason_null_ls_status_ok then
				return
			end
			require("mason-null-ls").setup({
				ensure_installed = {
					-- Linters
					"ansible-lint",
					"cfn-lint",
					"flake8",
					"jsonlint",
					"markdownlint",
					"shellcheck",
					"vint",
					"yamllint",
					-- Formatters
					"beautysh",
					"black",
					"prettier",
					"yamlfmt",
					"shfmt",
				},
				automatic_installation = true,
				handlers = {},
			})

			local null_ls_status_ok, null_ls = pcall(require, "null-ls")
			if not null_ls_status_ok then
				return
			end
			-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/formatting
			local formatting = null_ls.builtins.formatting
			-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/diagnostics
			local diagnostics = null_ls.builtins.diagnostics
			null_ls.setup({
				debug = false,
				sources = {
					-- diagnostics
					diagnostics.zsh,
					-- formatting
					formatting.stylua,
				},
			})
		end,
	},
	{
		"williamboman/mason.nvim",
		opts = {
			ui = { border = "single" },
		},
		config = function()
			local status_ok, _ = pcall(require, "mason")
			if not status_ok then
				return
			end
			require("mason").setup({
				ui = {
					icons = {
						package_installed = "✓",
						package_pending = "➜",
						package_uninstalled = "✗",
					},
				},
			})
		end,
	},
	{
		"hinell/lsp-timeout.nvim",
		dependencies = { "neovim/nvim-lspconfig" },
		init = function()
			vim.g.lspTimeoutConfig = {
				stopTimeout = 1000 * 60 * 5, -- ms, timeout before stopping all LSPs
				startTimeout = 1000 * 10, -- ms, timeout before restart
				silent = false, -- true to suppress notifications
				filetypes = {
					ignore = { -- filetypes to ignore; empty by default
						-- lsp-timeout is disabled completely
					}, -- for these filetypes
				},
			}
		end,
	},
	{
		"j-hui/fidget.nvim",
		tag = "legacy",
		opts = {
			--options
		},
		config = function()
			local status_ok, _ = pcall(require, "fidget")
			if not status_ok then
				return
			end
			require("fidget").setup({})
		end,
	},
}
