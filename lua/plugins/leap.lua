-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"ggandor/leap.nvim"     :: Buffer search on steroids
    s   :: Leap forward
    F   :: Leap backward
]]

return {
	"ggandor/leap.nvim",
	event = { "BufReadPre", "BufNewFile" },
	keys = {
		{ "s", "<Plug>(leap-forward-to)", desc = "Leap forward" },
		{ "F", "<Plug>(leap-backward-to)", desc = "Leap backward" },
	},
}
