-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --

--[[
"someone-stole-my-name/yaml-companion.nvim"    :: Get, set and autodetect YAML schemas in your buffers
]]

return {
	"someone-stole-my-name/yaml-companion.nvim",
	ft = { "yaml", "yml" },
	config = function()
		require("telescope").load_extension("yaml_schema")
	end,
}
