-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"nat-418/boole.nvim" :: Neovim plugin for toggling booleans, etc.
    <C-a> :: Increment
    <C-x> :: Decrement
]]

return {
	"nat-418/boole.nvim",
	event = { "BufReadPre", "BufNewFile" },
	opts = {
		mappings = {
			increment = "<C-a>",
			decrement = "<C-x>",
		},
		-- User defined loops
		additions = {
		},
	},
}
