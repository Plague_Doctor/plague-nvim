-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"tummetott/unimpaired.nvim"     :: LUA port of tpope's famous vim-unimpaired plugin
]]

return {
	"tummetott/unimpaired.nvim",
	event = { "BufReadPre", "BufNewFile" },
	opts = {
		keymaps = {
			tprevious = "[T",
			tnext = "]T",
			tfirst = false,
			tlast = false,
		},
	},
}
