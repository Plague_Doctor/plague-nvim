-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"nvim-treesitter/nvim-treesitter"   :: Nvim Treesitter configurations and abstraction layer
"RRethy/nvim-treesitter-endwise"    :: Tree-sitter aware alternative to tpope's vim-endwise
"windwp/nvim-ts-autotag"            :: Use treesitter to auto close and auto rename html tag
]]

return {
	{
		"nvim-treesitter/nvim-treesitter",
		dependencies = {
			"RRethy/nvim-treesitter-endwise",
		},
		version = false,
		event = { "VeryLazy" },
		cmd = { "TSUpdateSync", "TSUpdate", "TSInstall" },
		build = function()
			vim.cmd("TSUpdate")
		end,
		init = function(plugin)
			require("lazy.core.loader").add_to_rtp(plugin)
			require("nvim-treesitter.query_predicates")
		end,
		opts = {
			highlight = { enable = true },
			indent = { enable = true, disable = { "yaml", }, },
			ensure_installed = {
				"bash",
				"diff",
				"html",
				"javascript",
				"json",
				"lua",
				"markdown",
				"markdown_inline",
				"python",
				"regex",
				"vim",
				"yaml"
			},
			sync_install = true,
			auto_install = true,
			endwise = { enable = true },
			autopairs = { enable = true },
			autotag = { enable = true },
			context_commentstring = {
				enable = true,
				autocmd = false,
			},
		},
		config = function(_, opts)
			if type(opts.ensure_installed) == "table" then
				---@type table<string, boolean>
				local added = {}
				opts.ensure_installed = vim.tbl_filter(function(lang)
					if added[lang] then
						return false
					end
					added[lang] = true
					return true
				end, opts.ensure_installed)
			end
			require("nvim-treesitter.configs").setup(opts)
		end,
	},
	-- Automatically add closing tags for HTML and JSX
	{
		"windwp/nvim-ts-autotag",
		event = { "VeryLazy" },
		opts = {},
	},
}
