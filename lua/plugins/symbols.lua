-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"simrat39/symbols-outline.nvim"     :: A tree like view for symbols
    <F8>    :: Show outline
]]

return {
	"simrat39/symbols-outline.nvim",
	opts = {
		auto_close = true,
		width = 40,
	},
	keys = {
		{
			"<F8>",
			"<cmd>SymbolsOutline<cr>",
			desc = "Symbols Outline",
		},
	},
}
