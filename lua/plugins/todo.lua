-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"folke/todo-comments.nvim"  :: Highlight, list and search todo comments in your projects
]]

return {
	"folke/todo-comments.nvim",
	event = { "BufNewFile", "BufReadPre" },
	cmd = { "TodoTrouble", "TodoTelescope" },
	keys = {
		{
			"]t",
			function()
				require("todo-comments").jump_next()
			end,
			desc = "Next todo comment",
		},
		{
			"[t",
			function()
				require("todo-comments").jump_prev()
			end,
			desc = "Previous todo comment",
		},
		{ "<leader>xt", "<cmd>TodoTrouble<cr>", desc = "Todo (Trouble)" },
		{ "<leader>xT", "<cmd>TodoTrouble keywords=TODO,FIX,FIXME<cr>", desc = "Todo/Fix/Fixme (Trouble)" },
		{ "<leader>st", "<cmd>TodoTelescope<cr>", desc = "Todo" },
		{ "<leader>sT", "<cmd>TodoTelescope keywords=TODO,FIX,FIXME<cr>", desc = "Todo/Fix/Fixme" },
		{ "<leader>mm", "OFLAG:<ESCAPE><Plug>(comment_toggle_linewise_current)", desc = "Add Flag" },
	},
	opts = {
		keywords = {
			FLAG = { icon = "", color = "flag" },
		},
		colors = {
			-- flag = { "DiagnosticInfo", "DC2626" }
			flag = { "#54E709", "#54E709" },
		},
	},
	config = true,
}
