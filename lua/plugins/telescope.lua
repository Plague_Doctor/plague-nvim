-- ========================================================================== --
-- ==                          PLUGIN MANIFESTS                            == --
-- ========================================================================== --
--[[
"nvim-telescope/telescope.nvim"     :: Find, Filter, Preview, Pick. All lua, all the time
    <leader> ff    :: Find files
    <leader> fg    :: Live grep

"debugloop/telescope-undo.nvim"     :: Undo plugin for Telescope
"ghassan0/telescope-glyph.nvim"     :: Search font glyphs in Telescope
"ahmedkhalf/project.nvim"           :: The superior project management
]]

return {
	"nvim-telescope/telescope.nvim",
	version = false,
	cmd = { "Telescope" },
	keys = {
		{ "<leader>ff", "<cmd>Telescope find_files<cr>", desc = "Find Files" },
		{ "<leader>fg", "<cmd>Telescope live_grep<cr>", desc = "Live Grep" },
		{
			"<F9>",
			"<cmd>lua require('telescope.builtin').find_files({find_command={'fd', vim.fn.expand(\"<cword>\")}})<cr>",
			desc = "Find files under cursor",
		},
	},
	dependencies = {
		{
			"debugloop/telescope-undo.nvim",
			config = function()
				utils.on_load("telescope.nvim", function()
					require("telescope").load_extension("undo")
				end)
			end,
		},
		{
			"ghassan0/telescope-glyph.nvim",
			config = function()
				utils.on_load("telescope.nvim", function()
					require("telescope").load_extension("glyph")
				end)
			end,
		},
		{
			"ahmedkhalf/project.nvim",
			config = function()
				utils.on_load("telescope.nvim", function()
					require("telescope").load_extension("projects")
				end)
			end,
		},
	},
	opts = function()
		local actions = require("telescope.actions")
		return {
			defaults = {
				layout_strategy = "vertical",
				mappings = { i = { ["<esc>"] = actions.close } },
			},
			extensions = {
				undo = {
					use_delta = false,
					side_by_side = false,
					layout_strategy = "vertical",
					layout_config = {
						preview_height = 0.8,
					},
				},
			},
		}
	end,
}
