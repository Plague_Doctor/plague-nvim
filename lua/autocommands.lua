-- ========================================================================== --
-- ==                             AUTOCOMMANDS                             == --
-- ========================================================================== --

-- Generic Auto Commands
vim.cmd([[
  augroup _general_settings
      autocmd!
      autocmd FileType qf,help,man,lspinfo nnoremap <silent> <buffer> q :close<CR>
      autocmd TextYankPost * silent!lua require('vim.highlight').on_yank({higroup = 'Cursor', timeout = 350})
      autocmd BufWinEnter * :set formatoptions-=cro
      autocmd FileType qf set nobuflisted
  augroup end
]])

-- Show diagnostic on CursorHold
utils.ToggleDiagnostics()

-- Set highlighting for spell errors
utils.create_augroup({
	{ "ColorScheme", "*", "hi", "clear", "SpellBad" },
	{ "ColorScheme", "*", "hi", "SpellBad", "ctermfg=009", "ctermbg=011", "guifg=#ff0000", "guibg=#ffff00" },
}, "SpellBadHighlights")

-- Automatically opened all folds
utils.create_augroup({
	{ "BufWinEnter", "*", "silent!", ":%foldopen!" },
}, "OpenAllFolds")

-- Restore cursor position
vim.api.nvim_create_autocmd({ "BufReadPost" }, {
	pattern = { "*" },
	callback = function()
		vim.api.nvim_exec('silent! normal! g`"zv', false)
	end,
})
