-- ========================================================================== --
-- ==                           EDITOR SETTINGS                            == --
-- ========================================================================== --

utils.make_directory("~/.nvim/undo") -- Create directory if doesn't exist
utils.make_directory("~/.nvim/backup") -- Create directory if doesn't exist
utils.make_directory("~/.nvim/swap") -- Create directory if doesn't exist

local vim_options = {
	fileencodings = "utf-8", -- Set utf-8 file encoding
	hidden = true, -- Allow buffer switching without saving
	number = true, -- Line numbers on
	mouse = "a", -- Use mouse
	mousehide = true, -- Hide the mouse cursor while typing
	ignorecase = true, -- Case insensitive search
	smartcase = true, -- Case sensitive when uc present
	incsearch = true, -- Makes search act like search in modern browsers
	breakindent = true, -- Preserve the indentation of a virtual line
	tabstop = 2, -- number of visual spaces per TAB
	softtabstop = 2, -- number of spaces in tab when editing
	shiftwidth = 2, -- number of spaces to use for autoindent
	expandtab = true, -- tabs are space
	smarttab = true, -- Be smart when using tabs
	scrolljump = 5, -- Lines to scroll when cursor leaves screen
	scrolloff = 3, -- Minimum lines to keep above and below cursor
	lazyredraw = true, -- Only redraw when necesary
	autoindent = true, -- Use auto indentation
	smartindent = true, -- Use smart indentation
	copyindent = true, -- copy indent from the previous line
	pastetoggle = "<F10>", -- Sane indentation on pastes
	foldmethod = "indent", -- Set foldmethod to indent
	foldlevel = 20, -- Set default folding level
	foldlevelstart = 20, -- As above, but sets when nvim gets opened
	splitright = true, -- Puts new vsplit windows to the right of the current
	splitbelow = true, -- Puts new split windows to the bottom of the current
	list = true, -- Use list mode
	listchars = { tab = "␣ ", extends = "»", precedes = "«", nbsp = "·", trail = "•" },
	conceallevel = 0, -- so that `` is visible in markdown files
	hlsearch = true, -- highlight all matches on previous search pattern
	cmdheight = 2, -- more space in the neovim command line for displaying messages
	pumheight = 10, -- pop up menu height
	showmode = false, -- we don't need to see things like -- INSERT -- anymore
	signcolumn = "yes", -- always show the sign column, otherwise it would shift the text each time
	wrap = false, -- display lines as one long line
	clipboard = "unnamedplus", -- Allow to copy from/to clipboard
	completeopt = { "menu", "menuone", "noselect" },
	undodir = vim.fn.expand("~/.nvim/undo"), -- Undo directory location
	backupdir = vim.fn.expand("~/.nvim/backup"), -- Backup directory location
	directory = vim.fn.expand("~/.nvim/swap"), -- Swap files directory location
	backup = true, -- Backups are nice...
	undofile = true, -- Persist undo is even nicer
	undolevels = 1000, -- Maximum number of changes that can be undone
	undoreload = 5000, -- Maximum number lines to save for undo on a buffer reload
	cursorline = true, -- set cursorline on
	updatetime = 300, -- 'updatetime' for responsiveness
	termguicolors = true, -- Use Colours
	spell = false, -- Spell checking off
	spelllang = { "en_au", "pl" },
}

for k, v in pairs(vim_options) do
	vim.opt[k] = v
end
