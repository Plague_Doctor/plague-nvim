-- ========================================================================== --
-- ==                         FINAL CONFIGURATION                          == --
-- ========================================================================== --

-- Color Scheme configuration

vim.cmd("colorscheme kanagawa")
require("kanagawa")
-- vim.cmd('colorscheme OceanicNext')
-- require("kanagawa").load("dragon")
-- vim.cmd.colorscheme "catppuccin-mocha"
-- vim.cmd.colorscheme "catppuccin-macchiato"
-- vim.cmd.colorscheme "catppuccin-frappe"
