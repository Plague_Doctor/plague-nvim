--[[

   ██████╗ ██╗      █████╗  ██████╗ ██╗   ██╗███████╗       ███╗   ██╗██╗   ██╗██╗███╗   ███╗
   ██╔══██╗██║     ██╔══██╗██╔════╝ ██║   ██║██╔════╝       ████╗  ██║██║   ██║██║████╗ ████║
   ██████╔╝██║     ███████║██║  ███╗██║   ██║█████╗  █████╗ ██╔██╗ ██║██║   ██║██║██╔████╔██║
   ██╔═══╝ ██║     ██╔══██║██║   ██║██║   ██║██╔══╝  ╚════╝ ██║╚██╗██║╚██╗ ██╔╝██║██║╚██╔╝██║
   ██║     ███████╗██║  ██║╚██████╔╝╚██████╔╝███████╗       ██║ ╚████║ ╚████╔╝ ██║██║ ╚═╝ ██║
   ╚═╝     ╚══════╝╚═╝  ╚═╝ ╚═════╝  ╚═════╝ ╚══════╝       ╚═╝  ╚═══╝  ╚═══╝  ╚═╝╚═╝     ╚═╝

]]

require("utils")
require("options")
require("keymaps")
require("lazy-nvim")
require("autocommands")
require("final")

local status, personal = pcall(require, "personal")
if not status then
	return
else
  require("personal")
end


-- vim: tabstop=2 shiftwidth=2 expandtab
