#!/bin/bash

Plague_nVim_dir="$HOME/.plague-nvim"
nVim_dir="$HOME/.config/nvim"
Plague_nVim_git_url='https://gitlab.com/Plague_Doctor/plague-nvim.git'

display_notes() {
    cat << EOF
===========================================================

  Plague Doctor's plague-nvim project has been installed.

================== Manual Steps Required ==================

  In order to get all plugins to work properly, please
  follow steps below:

  1. Install dependencies:

    mercurial cargo fd

===========================================================
  ..:: 3.0 ::..                   ..:: Plague Doctor ::..

EOF
}

check_deps() {
    echo " ..:: Plague Doctor's neo-vim project :: Installation ::.."
    if [ "$HOME" = "" ]; then
        echo "  \$HOME is empty. Please fix before proceeding."
        echo "==========================================================="
        exit 1
    fi

    command -v nvim >/dev/null 2>&1 || { echo >&2 "nvim is not installed. Please fix before proceeding."; exit 1; }
    command -v git >/dev/null 2>&1 || { echo >&2 "git is not installed. Please fix before proceeding."; exit 1; }
}

clone_plague-nvim() {
    if [ ! -e "$Plague_nVim_dir" ]; then
        echo "==========================================================="
        echo "  Clonning plague-nvim repo..."
        git clone --recursive "$Plague_nVim_git_url" "$Plague_nVim_dir"
    else
        echo "==========================================================="
        echo "  Updating plague-nvim repo..."
        cd "$Plague_nVim_dir" || return
        git pull origin master
    fi
    cd "$Plague_nVim_dir" || return
}

create_structure() {
    if [[ -d "$nVim_dir" ]]; then
        echo "==========================================================="
        echo "Default directory \"$nVim_dir\" exists and will be removed now."
        rm -rf "$nVim_dir" && mkdir -p "$nVim_dir"
    else
        echo "==========================================================="
        echo "Default directory \"$nVim_dir\" does not exist. Creating..."
        mkdir -p "$nVim_dir"
    fi

    echo "==========================================================="
    echo "  Creating plague-nvim structure..."
    if [ ! -L "$nVim_dir/init.lua" ]; then ln -sf "$Plague_nVim_dir/init.lua" "$nVim_dir/init.lua"; fi
    if [ ! -L "$nVim_dir/lua" ]; then ln -sf "$Plague_nVim_dir/lua" "$nVim_dir/lua"; fi
    if [ ! -L "$nVim_dir/cheatsheet.txt" ]; then ln -sf "$Plague_nVim_dir/cheatsheet.txt" "$nVim_dir/cheatsheet.txt"; fi

    echo "==========================================================="
    echo "  Deploying rules..."
    if [ ! -f "$HOME/.config/yamllint/config" ]; then mkdir "$HOME/.config/yamllint"; cp "$Plague_nVim_dir/rules/yamllint" "$HOME/.config/yamllint/config"; fi
    if [ ! -f "$HOME/.flake8" ]; then cp "$Plague_nVim_dir/rules/flake8" "$HOME/.flake8"; fi
}


######################### #MAIN Function ##########################
clear
check_deps
clone_plague-nvim
create_structure
display_notes

export SHELL="$system_shell"


# vim: tabstop=2 shiftwidth=2 expandtab
