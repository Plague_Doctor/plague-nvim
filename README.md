```
   ██████╗ ██╗      █████╗  ██████╗ ██╗   ██╗███████╗       ███╗   ██╗██╗   ██╗██╗███╗   ███╗
   ██╔══██╗██║     ██╔══██╗██╔════╝ ██║   ██║██╔════╝       ████╗  ██║██║   ██║██║████╗ ████║
   ██████╔╝██║     ███████║██║  ███╗██║   ██║█████╗  █████╗ ██╔██╗ ██║██║   ██║██║██╔████╔██║
   ██╔═══╝ ██║     ██╔══██║██║   ██║██║   ██║██╔══╝  ╚════╝ ██║╚██╗██║╚██╗ ██╔╝██║██║╚██╔╝██║
   ██║     ███████╗██║  ██║╚██████╔╝╚██████╔╝███████╗       ██║ ╚████║ ╚████╔╝ ██║██║ ╚═╝ ██║
   ╚═╝     ╚══════╝╚═╝  ╚═╝ ╚═════╝  ╚═════╝ ╚══════╝       ╚═╝  ╚═══╝  ╚═══╝  ╚═╝╚═╝     ╚═╝
```

# plague-nvim Extension Project

## About project

The files provide a list of configurations for NeoVim I am personally interested in.
While most of the settings modified by this file are highly recommended and very usable, I strongly advise to customise them according to your needs.

## License

The plague-nvim Extension Project is licensed under [GPLv2](http://www.gnu.org/licenses/gpl-2.0.html).

### Prerequisites

#### General

- `fd` :: A simple, fast and user-friendly alternative to 'find'
- `ripgrep` :: Ripgrep recursively searches directories for a regex pattern while respecting your gitignore

#### Linters

- shellharden
- stylua
- ansible-lint
- yamllint
- eslint

#### LSP Servers

- vim-language-server
- lua-language-server
- ansible-language-server
- yaml-language-server
- jedi-language-server
- dockerfile-language-server
- cmake-language-server
- bash-language-server
- marksman-bin
- vscode-langservers-extracted

### Installation

The easiest way to install plague-nvim is to use provided installer.
Simply copying and pasting the following line into a terminal:

```bash
curl https://gitlab.com/Plague_Doctor/plague-nvim/raw/master/setup.sh -L -o - | bash
```

### Updating

In order to update project to the latest version, please copy and paste to the terminal:

```bash
cd $HOME/.plague-nvim && git pull && nvim +PackerSync && cd -
```

### Plugins

- [wbthomason/packer.nvim](https://github.com/wbthomason/packer.nvim)
- [mhartington/oceanic-next](https://github.com/mhartington/oceanic-next)
- [shaunsingh/nord.nvim](https://github.com/shaunsingh/nord.nvim)
- [nvim-lualine/lualine.nvim](https://github.com/nvim-lualine/lualine.nvim)
- [fidget.nvim](https://github.com/j-hui/fidget.nvim)
- [nvim-lua/plenary.nvim](https://github.com/nvim-lua/plenary.nvim)
- [nvim-treesitter/nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter)
- [nvim-neo-tree/neo-tree.nvim](https://github.com/nvim-neo-tree/neo-tree.nvim)
- [Tummetott/unimpaired.nvim](https://github.com/Tummetott/unimpaired.nvim)
- [altermo/ultimate-autopair.nvim](https://github.com/altermo/ultimate-autopair.nvim)
- [boole.nvim](https://github.com/nat-418/boole.nvim)
- [echasnovski/mini.surround](https://github.com/echasnovski/mini.surround)
- [echasnovski/mini.align](https://github.com/echasnovski/mini.align)
- [echasnovski/mini.trailspace](https://github.com/echasnovski/mini.trailspace)
- [echasnovski/mini.move](https://github.com/echasnovski/mini.move)
- [echasnovski/mini.indentscope](https://github.com/echasnovski/mini.indentscope)
- [ggandor/leap.nvim](https://github.com/ggandor/leap.nvim)
- [jiaoshijie/undotree](https://github.com/jiaoshijie/undotree)
- [numToStr/Comment.nvim](https://github.com/numToStr/Comment.nvim)
- [folke/todo-comments.nvim](https://github.com/folke/todo-comments.nvim)
- [nvim-telescope/telescope.nvim](https://github.com/nvim-telescope/telescope.nvim)
- [telescope-undo.nvim](https://github.com/debugloop/telescope-undo.nvim)
- [ghassan0/telescope-glyph.nvim](https://github.com/ghassan0/telescope-glyph.nvim)
- [lewis6991/gitsigns.nvim](https://github.com/lewis6991/gitsigns.nvim)
- [sindrets/diffview.nvim](https://github.com/sindrets/diffview.nvim)
- [Wansmer/treesj](https://github.com/Wansmer/treesj)
- [sudormrfbin/cheatsheet.nvim](https://github.com/sudormrfbin/cheatsheet.nvim)
- [simrat39/symbols-outline.nvim](https://github.com/simrat39/symbols-outline.nvim)
- [Pocco81/true-zen.nvim](https://github.com/Pocco81/true-zen.nvim)
- [folke/twilight.nvim](https://github.com/folke/twilight.nvim)
- [ahmedkhalf/project.nvim](https://github.com/ahmedkhalf/project.nvim)
- [hrsh7th/nvim-cmp](https://github.com/hrsh7th/nvim-cmp)
- [hrsh7th/cmp-buffer](https://github.com/hrsh7th/cmp-buffer)
- [hrsh7th/cmp-path](https://github.com/hrsh7th/cmp-path)
- [hrsh7th/cmp-cmdline](https://github.com/hrsh7th/cmp-cmdline)
- [dmitmel/cmp-cmdline-history](https://github.com/dmitmel/cmp-cmdline-history)
- [saadparwaiz1/cmp_luasnip](https://github.com/saadparwaiz1/cmp_luasnip)
- [L3MON4D3/LuaSnip](https://github.com/L3MON4D3/LuaSnip)
- [rafamadriz/friendly-snippets](https://github.com/rafamadriz/friendly-snippets)
- [williamboman/mason.nvim](https://github.com/rwilliamboman/mason.nvim)
- [williamboman/mason-lspconfig.nvim](https://github.com/rwilliamboman/mason-lspconfig.nvim)
- [jay-babu/mason-null-ls.nvim](https://github.com/rjay-babu/mason-null-ls.nvim)
- [neovim/nvim-lspconfig](https://github.com/neovim/nvim-lspconfig)
- [hrsh7th/cmp-nvim-lsp](https://github.com/hrsh7th/cmp-nvim-lsp)
- [jose-elias-alvarez/null-ls.nvim](https://github.com/jose-elias-alvarez/null-ls.nvim)

## Donate

If you like this project, please donate. You can send coins to the following addresses.

**Bitcoin**: 3KWsKEw3Ewu7vcTREjQUuR8LUf4QXoZEHK

**Litecoin**: MHaqGAqMoQGiTkNyg7w8haC6mU25Frgi3M
